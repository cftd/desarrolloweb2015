class CreateCanchas < ActiveRecord::Migration
  def change
    create_table :canchas do |t|
      t.string :tipo
      t.string :estado
      t.references :rd, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
