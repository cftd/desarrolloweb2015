class CreateRvas < ActiveRecord::Migration
  def change
    create_table :rvas do |t|
      t.string :nombre
      t.string :telefono
      t.date :fecha
      t.time :hora
      t.integer :valor
      t.references :cancha, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
