class Cancha < ActiveRecord::Base
  belongs_to :rd
  has_many :horarioreservas, dependent: :destroy
  validates :rd_id, presence: true
end
