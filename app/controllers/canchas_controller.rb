class CanchasController < ApplicationController
 

 def create
    @rd=Rd.find(params[:rd_id])
    @cancha= @rd.canchas.create(cancha_params)
    if @cancha.save
      
      flash[:success] = "cancha creado exitosamente"
       redirect_to rds_url
    else
      render 'new'
    end
  end
 

  


def destroy
    Cancha.find(params[:id]).destroy
    flash[:success] = "cancha Borrada"
    redirect_to rds_url
  end





def edit
    @cancha = Cancha.find(params[:id])
  end
  
  

def update
    @cancha = Cancha.find(params[:id])
    if @cancha.update_attributes(cancha_params)
      flash[:success] = "cancha actualizado correctamente"
      render 'edit'
       
    else
      render 'edit'
    end
  end
  
  private

    def cancha_params
      params.require(:cancha).permit(:tipo, :estado)
    end
end
