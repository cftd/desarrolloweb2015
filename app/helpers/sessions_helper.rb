module SessionsHelper
    def log_in(adm)
    session[:adm_id] = adm.id
    end
    
    def current_adm
    @current_adm ||= Adm.find_by(id: session[:adm_id])
   end
   
   def logged_in?
    !current_adm.nil?
  end
  
   def log_out
    session.delete(:adm_id)
    @current_adm = nil
  end
end