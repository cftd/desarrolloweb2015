class AddIndexToAdmsEmail < ActiveRecord::Migration
  def change
    add_index :adms, :email, unique: true
  end
end
