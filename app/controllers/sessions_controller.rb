class SessionsController < ApplicationController
  def new
  end
 def create
    adm = Adm.find_by(email: params[:session][:email].downcase)
    if adm && adm.authenticate(params[:session][:password])
      log_in(adm)
       redirect_to adms_url
    else
      flash.now[:danger] = 'Datos inválidos' 
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end
end