class CreateRds < ActiveRecord::Migration
  def change
    create_table :rds do |t|
      t.string :nombre
      t.string :direccion
      t.string :responsable

      t.timestamps null: false
    end
  end
end
