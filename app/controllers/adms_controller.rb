class AdmsController < ApplicationController
  before_action :logged_in_adm, only: [:index, :edit, :update, :new, :create]
  before_action :correct_adm,   only: [:edit, :update]
  before_action :palla
  def new
    @adm= Adm.new
  end
  
  def index
    @adms = Adm.all
  end
  
   def destroy
    Adm.find(params[:id]).destroy
    flash[:success] = "Administrador Borrado"
    redirect_to adms_url
  end
  
  
  def show
    @adm = Adm.find(params[:id])
  end
  
  def create
    @adm = Adm.new(adm_params)    
    if @adm.save
      #log_in @adm
      flash[:success] = "Adminstrador creado exitosamente"
       redirect_to new
    else
      render 'new'
    end
  end
  
  def edit
    @adm = Adm.find(params[:id])
  end

def update
    @adm = Adm.find(params[:id])
    if @adm.update_attributes(adm_params)
      flash[:success] = "Perfil actualizado correctamente"
      redirect_to @adm
    else
      render 'edit'
    end
  end
  
  private

    def adm_params
      params.require(:adm).permit(:nombre, :email, :roles,:password,
                                   :password_confirmation)
    end
    
    def logged_in_adm #evita que desde afuera modifiquen#
      unless logged_in?
        flash[:danger] = "Por favor ingrese."
        redirect_to login_url
      end
    end
    
     def correct_adm #evita que desde adentro modifiquen#
      @adm = Adm.find(params[:id])
      redirect_to(root_url) unless @adm == current_adm
    end
    
     def palla
      if logged_in?
    @current_user = current_adm
    if @current_user.roles==1
      redirect_to(rvas_path)
    end
  end
end
end
