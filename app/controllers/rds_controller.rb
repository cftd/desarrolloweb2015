class RdsController < ApplicationController
before_action :logged_in_adm, only: [:index, :edit, :update, :create]   
    
    
  def new
    @rd= Rd.new
  end
  
   def create
    @rd = Rd.new(rd_params)    
    if @rd.save
      #log_in @adm
      flash[:success] = "Recinto deportivo creado exitosamente"
       redirect_to new
    else
      render 'new'
    end
  end
  
   def index
   @rds= Rd.all
end

def destroy
    Rd.find(params[:id]).destroy
    flash[:success] = "Recinto deportivo Borrado"
    redirect_to rds_url
  end
  
  def show
  @rd=Rd.find(params[:id])
  @canchas=@rd.canchas
  
end
def edit
    @rd = Rd.find(params[:id])
  end

def update
    @rd = Rd.find(params[:id])
    if @rd.update_attributes(rd_params)
      flash[:success] = "Recinto deportivo actualizado correctamente"
      redirect_to new
    else
      render 'edit'
    end
  end
  
def mostrarLista
   @rds=Rd.all
end
  
def mostrarTabla
  
     @canchas = Cancha.where("rd_id = ?", params[:lista])
    #redirect a otro metodo con arreglo de canchas y dia seleccionado, otro metodo recibe el valor del dia
    @fecha=  Date.civil(*params[:fecha].sort.map(&:last).map(&:to_i))
   @reservas= Rva.where("fecha= ?", @fecha)
   #@reservita= Rva.all
   
   
   #@fecha2=  params[:fecha]
   
end
  
  
 private

    def rd_params
      params.require(:rd).permit(:nombre, :direccion, :responsable)
    end
    
    def logged_in_adm #evita que desde afuera modifiquen#
      unless logged_in?
        flash[:danger] = "Por favor ingrese."
        redirect_to login_url
      end
    end
    
end
