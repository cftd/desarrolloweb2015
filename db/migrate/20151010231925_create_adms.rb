class CreateAdms < ActiveRecord::Migration
  def change
    create_table :adms do |t|
      t.string :nombre
      t.string :email

      t.timestamps null: false
    end
  end
end
