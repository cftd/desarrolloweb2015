class CreateReservs < ActiveRecord::Migration
  def change
    create_table :reservs do |t|
      t.string :nombre
      t.integer :telefono
      t.datetime :finicio
      t.datetime :ffin
      t.integer :valor
      t.references :cancha, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
