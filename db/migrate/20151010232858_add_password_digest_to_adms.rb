class AddPasswordDigestToAdms < ActiveRecord::Migration
  def change
    add_column :adms, :password_digest, :string
  end
end
