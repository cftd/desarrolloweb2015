class CreateReservas < ActiveRecord::Migration
  def change
    create_table :reservas do |t|
      t.string :nombre
      t.integer :telefono
      t.datetime :finicio
      t.datetime :ffin
      t.integer :valor

      t.timestamps null: false
    end
  end
end
