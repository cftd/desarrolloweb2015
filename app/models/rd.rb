class Rd < ActiveRecord::Base
    has_many :canchas, dependent: :destroy
     validates :nombre, presence: true, length: { maximum: 50 }
     validates :direccion, presence: true, length: { maximum: 50 }
     validates :responsable, presence: true, length: { maximum: 50 }
end
